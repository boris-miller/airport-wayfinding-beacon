//
//  ViewController.swift
//  BeaconWF
//
//  Created by Boris Miller on 3/1/18.
//  Copyright © 2018 Outware. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController, CLLocationManagerDelegate {
  let locationManager = CLLocationManager()

  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    locationManager.delegate = self;
    locationManager.requestAlwaysAuthorization()
  }

  // MARK: CLLocationManagerDelegate
  
  func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    if status == .authorizedAlways || status == .authorizedWhenInUse {
      if CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self) {
        if CLLocationManager.isRangingAvailable() {
          startScanning()
        }
      }
    }
    else
    {
      locationManager.requestAlwaysAuthorization()
    }
  }
  
  func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
    print(beacons)
    
    for beacon in beacons {
      let proximity = beacon.proximity
      if proximity == .unknown { continue }
      print("proximity \(proximity.rawValue) accuracy \(beacon.accuracy) rssi \(beacon.rssi)")
      // The distance formula based on the nex white paper - https://www.rn.inf.tu-dresden.de/dargie/papers/icwcuca.pdf
      let txPower = -59 // Radius Networks 2F234454 txPower
      let signalPropagation = 2.0 //  the signal propagation constant or exponent (depends on the broadcast frequency and environment)
      let distance = pow(10.0, (Double(txPower) - Double(beacon.rssi)) / (signalPropagation * 10.0))
      print("distance = \(distance)")
    }
  }
  
  // MARK: Private methods

  private func startScanning() {
    let proximityUUID = UUID(uuidString: "2F234454-CF6D-4A0F-ADF2-F4911BA9FFA6")! // Radius Networks 2F234454
//    let uuid = UUID(uuidString: "B9407F30-F5F8-466E-AFF9-25556B57FE6D")!
//    let beaconRegion = CLBeaconRegion(proximityUUID: uuid, major: 20, minor: 2, identifier: "Estimotes")
//    let beaconRegion = CLBeaconRegion(proximityUUID: uuid, identifier: "Estimotes")
    let beaconRegion = CLBeaconRegion(proximityUUID: proximityUUID, identifier: "Radius Networks 2F234454")
    locationManager.startMonitoring(for: beaconRegion)
    locationManager.startRangingBeacons(in: beaconRegion)
  }

}

